package com.example.examplemod;

import com.mojang.logging.LogUtils;
import com.seibel.distanthorizons.api.DhApi;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.slf4j.Logger;

/**
 * This is the Forge "Hello World" project
 * for the Distant Horizons API. <br>
 * This example is designed to be a jumping-off
 * point, so you can start using the DH API.
 * 
 * @version 2023-6-22
 */
@Mod("examplemod")
public class ExampleMod
{
    // Directly reference a slf4j logger
    private static final Logger LOGGER = LogUtils.getLogger();

    public ExampleMod()
    {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event)
    {
		// API versioning //
		
		LOGGER.info("Attempting to use the Distant Horizons API...");
		
		// DH Version
		String dhVersion = DhApi.getModVersion();
		LOGGER.info("DH version: " + dhVersion);
		// API version
		int dhApiMajorVersion = DhApi.getApiMajorVersion();
		int dhApiMinorVersion = DhApi.getApiMinorVersion();
		LOGGER.info("DH API version: " + dhApiMajorVersion + "." + dhApiMinorVersion);
		
	}
	
}
