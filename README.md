# <img src="/readmeImages/Distant-Horizons-API.png" height="150"> <br> Distant Horizons Api Examples

This repo contains a few basic examples on how to use the Distant Horizons Api.

- Minecraft version: 1.18.2
- Built with: JDK 21
    - (Although other JDK's may also work)
  

## Included Examples:
- **Fabric**
  - Includes:
    - Basic Gradle setup
    - Accessing DH's version and API version.
    - Event binding/handling
    - Config value getting/setting
    - Terrain data getting (specifically via ray-cast)
    - World generator overriding
      - Chunk sized world gen
      - N-sized world gen
    - Generic Rendering 
      - AKA a way to render cube based objects into DH's terrain
- **Forge**
    - Includes:
        - Basic Gradle setup
        - Accessing DH's version and API version.

Here's a picture of the generic rendering (`the beacons and floating brightly-colored cubes`) and N-sized world generator (`red, orange, yellow, and green LODs in the distance`) API examples in game:
![InGame.png](readmeImages/InGame.png)

### Why doesn't Forge include all the examples Fabric does?

The Distant Horizons API is Minecraft and mod loader agnostic, in other words: its usage is the same regardless of Minecraft version or mod loader. For example: getting DH's version number is done identically on both Forge and Fabric. So, to prevent duplicate code the examples will only be included in the Fabric example project.

The one exception to this rule is whenever the API returns or accepts Minecraft objects; like Chunks in the world generator or BlockStates in the terrain data repo. In those cases the objects being passed in or returned will have different names based on the mappings your mod loader uses.


## Basic Gradle Setup

Full examples can be seen in the Forge and Fabric projects included in this repo. However, for a more in-depth walkthrough of the gradle setup process, please view the [DH Developer Wiki](https://gitlab.com/distant-horizons-team/distant-horizons-core/-/wikis/home).


## JavaDocs

The API jars should include the source files necessary to view JavaDocs while in your IDE, however if they don't appear right away you include download/link them using your IDE's internal tools. (In Intellij this is done by opening one of the API classes and clicking the "Download Sources" or "Choose Sources" button that appears at the top.)

Alternatively, you can view the docs online: <br>
https://distant-horizons-team.gitlab.io/distant-horizons/ 


### Additional Notes

If there are any issues with any of the example projects or something is missing please create an issue in the [main repo's issue tracker](https://gitlab.com/distant-horizons-team/distant-horizons/-/issues).

If you need additional help or want to share what you've made please join our [Discord](https://discord.gg/xAB8G4cENx).
