package DhApi;

import com.mojang.logging.LogUtils;
import com.seibel.distanthorizons.api.DhApi;
import com.seibel.distanthorizons.api.enums.EDhApiDetailLevel;
import com.seibel.distanthorizons.api.enums.worldGeneration.EDhApiDistantGeneratorMode;
import com.seibel.distanthorizons.api.enums.worldGeneration.EDhApiWorldGeneratorReturnType;
import com.seibel.distanthorizons.api.interfaces.block.IDhApiBiomeWrapper;
import com.seibel.distanthorizons.api.interfaces.block.IDhApiBlockStateWrapper;
import com.seibel.distanthorizons.api.interfaces.override.worldGenerator.IDhApiWorldGenerator;
import com.seibel.distanthorizons.api.interfaces.world.IDhApiLevelWrapper;
import com.seibel.distanthorizons.api.objects.data.DhApiTerrainDataPoint;
import com.seibel.distanthorizons.api.objects.data.IDhApiFullDataSource;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

/**
 * Unlike the {@link CustomChunkWorldGenerator} this world generator
 * doesn't generate Minecraft chunks, it generates LODs directly at
 * whatever detail level is requested. <br>
 * This allows for <b>extremely</b> fast world generation if properly implemented.
 * 
 * @version 2025-01-27 
 * @see CustomChunkWorldGenerator
 */
public class NSizedWorldGenerator implements IDhApiWorldGenerator
{
	private static final Logger LOGGER = LogUtils.getLogger();
	
	private final IDhApiLevelWrapper levelWrapper;
	
	
	
	//=============//
	// constructor //
	//=============//
	
	private NSizedWorldGenerator(IDhApiLevelWrapper levelWrapper)
	{ this.levelWrapper = levelWrapper; }
	
	/** This could also be handled externally from the {@link CustomChunkWorldGenerator} object */
	public static void registerForLevel(IDhApiLevelWrapper levelWrapper)
	{
		// override the core DH world generator for this level
		IDhApiWorldGenerator exampleWorldGen = new NSizedWorldGenerator(levelWrapper);
		DhApi.worldGenOverrides.registerWorldGeneratorOverride(levelWrapper, exampleWorldGen);
		
	}
	
	
	
	//============//
	// properties //
	//============//
	
	@Override
	public byte getSmallestDataDetailLevel() { return (byte) (EDhApiDetailLevel.BLOCK.detailLevel); }
	@Override
	public byte getLargestDataDetailLevel()
	{
		// This value determines the largest LOD area that can be generated
		// in this example 12 is equivalent to 2^12 (4096) blocks wide per LOD datapoint.
		// Try changing this number and watch how the LODs generated will generate at a higher detail level. 
		return (byte) (EDhApiDetailLevel.BLOCK.detailLevel + 12); 
	}
	
	
	@Override
	public EDhApiWorldGeneratorReturnType getReturnType() { return EDhApiWorldGeneratorReturnType.API_DATA_SOURCES; }
	
	@Override
	public boolean runApiValidation() { return true; }
	
	
	
	//==================//
	// chunk generation //
	//==================//
	
	@Override
	public CompletableFuture<Void> generateLod(
			int chunkPosMinX, int chunkPosMinZ,
			int posX, int posZ, byte detailLevel,
			IDhApiFullDataSource pooledFullDataSource,
			EDhApiDistantGeneratorMode generatorMode, ExecutorService worldGeneratorThreadPool,
			Consumer<IDhApiFullDataSource> resultConsumer)
	{
		return CompletableFuture.runAsync(() ->
			this.generateInternal(
				chunkPosMinX, chunkPosMinZ,
				posX, posZ, detailLevel,
				pooledFullDataSource, generatorMode, resultConsumer),
			worldGeneratorThreadPool);
	}
	public void generateInternal(
			int chunkPosMinX, int chunkPosMinZ,
			int posX, int posZ, byte detailLevel,
			IDhApiFullDataSource pooledFullDataSource,
			EDhApiDistantGeneratorMode generatorMode,
			Consumer<IDhApiFullDataSource> resultConsumer)
	{
		// get the blocks used for this position
		IDhApiBiomeWrapper biome;
		IDhApiBlockStateWrapper colorBlock;
		IDhApiBlockStateWrapper borderBlock;
		IDhApiBlockStateWrapper airBlock;
		int maxHeight;
		try
		{
			// for simplicity we're just going to use the "Plains" biome
			biome = DhApi.Delayed.wrapperFactory.getBiomeWrapper("minecraft:plains", this.levelWrapper);
			airBlock = DhApi.Delayed.wrapperFactory.getAirBlockStateWrapper();
			// we're going to have a stone border between each LOD generated so
			// you can easily see the LOD separation
			borderBlock = DhApi.Delayed.wrapperFactory.getDefaultBlockStateWrapper("minecraft:stone", this.levelWrapper);
			
			// for this example we're using different colored wool (and heights)
			// to show the different detail levels generated
			String blockResourceLocation;
			switch (detailLevel)
			{
				case 0:
					blockResourceLocation = "minecraft:red_wool";
					maxHeight = 60;
					break;
				case 1:
					blockResourceLocation = "minecraft:orange_wool";
					maxHeight = 70;
					break;
				case 2:
					blockResourceLocation = "minecraft:yellow_wool";
					maxHeight = 80;
					break;
				case 3:
					blockResourceLocation = "minecraft:lime_wool";
					maxHeight = 90;
					break;
				case 4:
					blockResourceLocation = "minecraft:cyan_wool";
					maxHeight = 100;
					break;
				case 5:
					blockResourceLocation = "minecraft:blue_wool";
					maxHeight = 100;
					break;
				case 6:
					blockResourceLocation = "minecraft:magenta_wool";
					maxHeight = 110;
					break;
				case 7:
					blockResourceLocation = "minecraft:white_wool";
					maxHeight = 120;
					break;
				case 8:
					blockResourceLocation = "minecraft:gray_wool";
					maxHeight = 120;
					break;
				default:
					blockResourceLocation = "minecraft:black_wool";
					maxHeight = 140;
					break;
			}
			
			colorBlock = DhApi.Delayed.wrapperFactory.getDefaultBlockStateWrapper(blockResourceLocation, this.levelWrapper);
			
		}
		catch (IOException e)
		{
			LOGGER.error("Failed to get biome/block: ["+ e.getMessage()+"].", e);
			return;
		}
		
		
		// create the LOD for this position
		ArrayList<DhApiTerrainDataPoint> dataPoints = new ArrayList<>();
		int width = pooledFullDataSource.getWidthInDataColumns();
		for (int x = 0; x < width; x++)
		{
			for (int z = 0; z < width; z++)
			{
				dataPoints.clear();
				
				// have the LOD filled with colorBlock,
				// with a thin border of borderBlock
				IDhApiBlockStateWrapper block = colorBlock;
				if (x == 0 || x == (width-1)
					|| z == 0 || z == (width-1))
				{
					block = borderBlock;
				}
				
				// Sky lighting can be ignored (IE left at 0). 
				// DH will auto light the LODs after the LOD's been submitted.
				// Block lighting however will need to be generated here if you have any.
				dataPoints.add(DhApiTerrainDataPoint.create((byte)0, 0, 0, 0, maxHeight, block, biome));
				// air must be generated otherwise Lighting won't propagate correctly
				dataPoints.add(DhApiTerrainDataPoint.create((byte)0, 0, 0, maxHeight, 256, airBlock, biome));
				
				pooledFullDataSource.setApiDataPointColumn(x, z, dataPoints);
			}
		}
		
		// Note: do NOT keep a reference to pooledFullDataSource outside of this method or thread
		// doing so may cause concurrent modifications and corrupted data.
		resultConsumer.accept(pooledFullDataSource);
	}
	
	
	@Override
	public void preGeneratorTaskStart() { /* do nothing */ }
	
	
	
	//=========//
	// cleanup //
	//=========//
	
	@Override
	public void close() { /* do nothing */ }
	
}
